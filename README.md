# boom-headshot
A tool to train Stable Diffusion on custom images, ostensibly to create headshots.  For way more detail on fine tuning Stable Diffusion see [this blog post](https://tryolabs.com/blog/2022/10/25/the-guide-to-fine-tuning-stable-diffusion-with-your-own-images)

[![headshot](headshot.gif)](https://www.youtube.com/watch?v=5_Xdi4T8mgI)

## Requirements

You will need a decent GPU with at least 16GB of VRAM and training times scale with GPU capabilites.  Training on a 3090 takes about an hour.

## Arguments

```
usage: finetune.py [-h] [--class-name CLASS_NAME] [--token TOKEN] [--repo REPO]

options:
  -h, --help            show this help message and exit
  --class-name CLASS_NAME
                        Class of the subject being trained, such as 'dog', 'man', 'woman', etc. Default is person
  --token TOKEN         Unique token that you will tell Stable Diffusion what the subject is. Ex: ManGuyFieri or Spotthedog.
                        Default is sksks
  --repo REPO           Override the default model used for fine tuning
```




## Workflow

 1. Select 10-12 images and crop them to be as close to square as possible. They will be resized to a sqare aspect ratio during training.
 2. Run `python3 finetune.py`  and follow the directions to copy your images to the training directory.
 3. Wait for training to complete
 4. Move your fine tuned model to the Stable Diffusion `models` directory.
    * The assumption is that you're using the [Automatic111](https://github.com/AUTOMATIC1111/stable-diffusion-webui) repo and know where to save models
 5. Generate an image using your class name and token. 
    * For instance if you trained on your cat named Sohpie with the `token` cutesophiecat the prompt might be "A film photograph of a cat cutesophiecat"

## Warning on disk space

The Docker container is 7.25GB and there isn't much I can do about that.  Training can result in *a lot* of disk space being used.  I have tweaked settings such that the training should™ result in ~50GB of files that will be removed as soon as training completes.

## Other Options

This repo bases the fine tuning on the [CompVis/stable-diffusion-v1-4](https://huggingface.co/CompVis/stable-diffusion-v1-4) repo.  You can modify this by passing the `--repo` argument with another huggingface repo, such as [wavymulder/Analog-Diffusion](https://huggingface.co/wavymulder/Analog-Diffusion).
