#!/bin/bash

output_dir=$1
token_name=$2
base_dir=$3

if ! docker image ls | grep diffusion-trainer; then
        echo "Training container not found.  Please wait while it gets built";
        docker build -t diffusion-trainer ./docker/;
fi

echo $train_cmd;


docker run --rm --gpus=all -v $(pwd):/training --workdir=/training diffusion-trainer bash fine_tune.sh; 
docker run --rm --gpus=all -v $(pwd):/training --workdir=/training diffusion-trainer python3 convert_diffusers_to_original_stable_diffusion.py --model_path=$output_dir --checkpoint_path=$base_dir/$token_name.ckpt
docker run -v $(pwd):/training --workdir=/training diffusion-trainer rm -rf /training/data/checkpoints
docker run -v $(pwd):/training --workdir=/training diffusion-trainer rm -rf /training/data/training

echo "Done! The pretrained model was saved to $base_dir/$token_name.ckpt"