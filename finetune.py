import os
from pathlib import Path
from time import sleep
import argparse


def make_args():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "--class-name",
        type=str,
        help="Class of the subject being trained, such as 'dog', 'man', 'woman', etc. Default is person",
        default="person",
    )
    parser.add_argument(
        "--token",
        type=str,
        help="Unique token that you will tell Stable Diffusion what the subject is. Ex: ManGuyFieri or Spotthedog. Default is sksks",
        default="sksks",
    )
    parser.add_argument(
        "--repo",
        type=str,
        help="Override the default model used for fine tuning",
        default="CompVis/stable-diffusion-v1-4",
    )
    parser.add_argument(
        "--max-training-steps",
        type=int,
        help="Max steps to use in training.  Too many will overfit the model.  A rule of thumb is 200 * Number of sample images. Default is 2400",
        default=2400
    )
    parser.add_argument(
        "--class-images",
        type=int,
        help="Number of class images to generate.  Default is 200",
        default=200
    )
    return parser.parse_args()


def main():
    args = make_args()
    # A general name for class like dog for dog images.
    class_name = args.class_name
    # A rare token name which will reference the subject.
    token_name = args.token

    base_dir = Path("./data")
    class_dir = base_dir / "training" / class_name

    instance_dir = base_dir / "training" / token_name
    output_dir = base_dir / "checkpoints"

    if not os.path.exists(instance_dir):
        os.makedirs(instance_dir)
    if not os.path.exists(output_dir):
        os.makedirs(output_dir)
    print("[*] Images must be sqaure. For best results use 10-12 imgages.")
    print(f"[*] Please place your training images in {instance_dir}")
    input("[*] Press enter once the images are ready and training will begin.")
    sleep(2)
    print(f"[*] Final checkpoint will be saved at {str(base_dir)}/{args.token}.ckpt\n")

    # ugly af but necessary because docker refuses to cleanly pass args
    training_command = f'python3 train_dreambooth.py --pretrained_model_name_or_path={args.repo} --instance_data_dir="{instance_dir}" --class_data_dir="{class_dir}" --output_dir="{output_dir}" --with_prior_preservation --prior_loss_weight=1.0 --instance_prompt="a {token_name} {class_name}" --class_prompt="photo of a {class_name}" --seed=1337 --resolution=512 --train_batch_size=1 --mixed_precision=fp16 --gradient_accumulation_steps=2 --learning_rate=1e-6 --lr_scheduler=constant --lr_warmup_steps=0 --num_class_images={args.class_images} --sample_batch_size=4 --max_train_steps={args.max_training_steps}'
    with open("fine_tune.sh", "w") as bashfile:
        bashfile.write(training_command)

    cmd = f"bash ./call_training.sh {output_dir} {token_name} {base_dir}"
    sleep(2)
    os.system(cmd)


if __name__ == "__main__":
    main()
